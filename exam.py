"""
   Nbody integration of N particles in N-body units from t=0 to
   t_end=1 Myr.  The initial cluster is a King (1966) model with
   dimension-less depth of the potential of W0=7. The initial
   distribution of stars is in virial equilibrium.  At this moment a
   4th order Hermite integrator is used for the integration.  Stellar
   masses are selected randomly from a Salpeter initial mass function
   between a minimum mass of Mmin=0.1MSun and Mmax=100MSun.  In order
   to assure proper scaling to astrophysical units, we have to define
   the cluster radius in physical units, in this case, we opted for a
   virial radius of 1pc.
   cluster in orbit in static potential
"""
import math
import numpy
from amuse.lab import *
from amuse.couple import bridge
from amuse.units.optparse import OptionParser
from amuse.units import quantities
from amuse.ext.galactic_potentials import MWpotentialBovy2015
from amuse.ext.protodisk import ProtoPlanetaryDisk
from matplotlib import pyplot

def evolve_cluster_in_potential(gravity, t_end, dt, channel_to_framework1, channel_to_framework2):
    
    Etot_init = gravity.kinetic_energy + gravity.potential_energy
    Etot_prev = Etot_init

    time = 0.0 | t_end.unit
    x = []
    y = []
    vx = []
    vy = []
    while time < t_end:
        time += dt

        gravity.evolve_model(time)
        channel_to_framework1.copy()
        channel_to_framework2.copy()
        x.append(gravity.particles[0].x.value_in(units.kpc))
        y.append(gravity.particles[0].y.value_in(units.kpc))
        vx.append(gravity.particles[0].vx.value_in(units.kms))
	vy.append(gravity.particles[0].vy.value_in(units.kms))

        Etot_prev_se = gravity.kinetic_energy + gravity.potential_energy

        Ekin = gravity.kinetic_energy 
        Epot = gravity.potential_energy
        Etot = Ekin + Epot
        """
        print "T=", time, 
        print "E= ", Etot, "Q= ", Ekin/Epot,
        print "dE=", (Etot_init-Etot)/Etot, "ddE=", (Etot_prev-Etot)/Etot 
        """
        Etot_prev = Etot

    return x, y, vx, vy


def integrate_totalsystem(stars, ramaparticles, t_end, dt, converter):
    MWG = MWpotentialBovy2015() #We set the Milky way potential as the Bovy2015 model

    cluster_gravity = BHTree(converter) #Make a gravity code for the cluster of stars
    disk_gravity = ph4(converter) #Make a gravity code for the disk
    cluster_gravity.particles.add_particles(stars) #Add the clusters stars to the cluster gravity code
    disk_gravity.particles.add_particles(ramaparticles) #Add the ramaparticles to the disk graviy code
    channel_from_cluster_gravity_to_framework = cluster_gravity.particles.new_channel_to(stars) #
    channel_from_disk_gravity_to_framework = disk_gravity.particles.new_channel_to(ramaparticles)
    
    #bridging, the milkyway has an effect on the cluster and the milkyway + cluster have an effect on the disk
    gravity = bridge.Bridge(use_threading=False)
    gravity.add_system(cluster_gravity, (MWG,))
    gravity.add_system(disk_gravity, (cluster_gravity, MWG))

    gravity.timestep = min(dt, 10|units.Myr)

    #Channel the gravity codes and evolve the cluster + the disk
    x,y = evolve_cluster_in_potential(gravity,t_end,dt,channel_from_cluster_gravity_to_framework,channel_from_disk_gravity_to_framework)
    gravity.stop()
    return x, y

def main(N, t_end, n_steps, Mtot, Rvir):
    numpy.random.seed(111)
    converter=nbody_system.nbody_to_si(Mtot, Rvir)
    dt = t_end/float(n_steps)
    

    #create a star cluster
    eps2 = (100 | units.AU)**2
    cluster = new_plummer_model(N, convert_nbody=converter)
    cluster.mass=new_salpeter_mass_distribution(len(cluster),.1|units.MSun,10.|units.MSun)
    cluster.scale_to_standard(convert_nbody=converter, smoothing_length_squared = eps2)
    cluster.position = [-7977,2764,-106.4] | units.parsec
    cluster.velocity = [88.45, 212.7, -2.788] | units.kms
    cluster.radius = Rvir

    disk = ProtoPlanetaryDisk(1000, convert_nbody=converter, densitypower=1.5, Rmin=600, Rmax=1100, q_out=1.0, discfraction=0.1).result
    
    x, y, vx, vy = integrate_totalsystem(cluster, disk, t_end, dt, converter)

    pyplot.scatter(cluster.x.value_in(units.kpc),cluster.y.value_in(units.kpc),c='c',lw=0,alpha=0.25)

    meanrelvelocity = []
    medianrelvelocity = []
    dispersonrelvelocity = []
    timeaxis = []

    t_step = 0.1|units.Gyr
    evolvetime = 0|units.Gyr
    
    while evolvetime <= t_end:
	x, y = integrate_totalsystem(cluster, disk, t_step, dt, converter)
	relvelocity = []
        for i in range(100):
 		distances[i] = []
		for j in range(100,1099):
			distances[i] = distances[i].append(((x[i]-x[j])**2+(y[i]-y[j])**2)**1/2) #distance array between star and rama particles
		closestraja = distances[i].argmin() + 100 #Which index of the array has the smallest distance we add 100 to get the index in the orinal array
		relvelocity = relvelocity.append(vx[i]-vx[closestraja],vy[i]-vy[closestraja]) #the relative velocity of the ith star and its closest raja neighbour
        meanrelvelocity = meanrelvelocity.append(numpy.mean(relvelocity))
	medianrelvelocity = medianrelvelocity.append(numpy.median(relvelocity))
	dispersionrelvelocity = dispersionrelvelocity.append(numpy.std(relvelocity))
	timeaxis = timeaxis.append(evolvetime)
	evolvetime = evolvetime + t_step

    plt.figure(1)
    plt.subplot(211)
    plt.plot(timexaxis, meanrelvelocity)
    plt.subplot(212)
    plt.plot(timexaxis, medianrelvelocity)
    plt.subplot(213)
    plt.plot(timexaxis, dispersionrelvelocity)
	
    
    
def new_option_parser():
    result = OptionParser()
    result.add_option("-N", dest="N", type="int",default = 100,
                      help="number of stars [%default]")
    result.add_option("-t", unit= units.Gyr,
                      dest="t_end", type="float", default = 4.6 | units.Gyr,
                      help="end time of the simulation [%default]")
    result.add_option("-n", dest="n_steps", type="float", default = 2,
                      help="number of diagnostics time steps [%default]")
    result.add_option("-M", unit=units.MSun,
                      dest="Mtot", type="float",default = 100 | units.MSun,
                      help="cluster mass [%default]")
    result.add_option("-R", unit= units.parsec,
                      dest="Rvir", type="float",default = 10 | units.parsec,
                      help="cluster virial radius [%default]")
    return result

if __name__ in ('__main__', '__plot__'):
    runs = 2
    main(100,4.6 | units.Gyr,10,100 | units.MSun,10 | units.parsec)


